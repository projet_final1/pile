variable "env" {
    type = string
}

variable "location" {}
variable "resource_group_name" {}
variable "machines_names" {}
variable "machines_types" {}
variable "machines_number" {}
variable "private_ips" {}
variable "user_name" {}
variable "domain_name" {}


// Secrets
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "public_key" {}