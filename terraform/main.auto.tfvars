  
location            = "francecentral"
resource_group_name = "jpr1"

// nombre de machines virtuelles créées
machines_number     = 2
// nom de chaque machine respectivement
machines_names      = [
    "server",
    "mongo"
]
// type de chaque machine
machines_types      = [
    "Standard_B1s",
    "Standard_B1s"
]
// ip privée de chauqe machine
private_ips         = [
    "10.0.1.4",
    "10.0.1.5"
]
// nom de l'utilisateur créé sur les machines
user_name           = "jpr"
// domain name sur le cloud azure
domain_name         = "jpr-devops"

// l'IP de la PIC sera <domain_name>-<env>.<location>.cloudapp.azure.com
