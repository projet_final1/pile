FROM centos:7
RUN yum -y install git unzip ansible
RUN curl -s -LO https://releases.hashicorp.com/terraform/0.12.21/terraform_0.12.21_linux_amd64.zip
RUN unzip terraform_0.12.21_linux_amd64.zip -d /usr/local/bin/
RUN mkdir ~/.ssh/
RUN echo "Host *" >> ~/.ssh/config
RUN echo "StrictHostKeyChecking no" >> ~/.ssh/config
RUN echo "UserKnownHostsFile=/dev/null" >> ~/.ssh/config
RUN echo "IdentityFile ~/.ssh/jprkey" >> ~/.ssh/config
RUN echo "Host server-test" >> ~/.ssh/config
RUN echo "Hostname jpr-devops-test.francecentral.cloudapp.azure.com" >> ~/.ssh/config
RUN echo "User jpr" >> ~/.ssh/config
RUN echo "IdentityFile ~/.ssh/jprkey" >> ~/.ssh/config
RUN echo "Host mongo-test" >> ~/.ssh/config
RUN echo "Hostname 10.0.1.5" >> ~/.ssh/config
RUN echo "User jpr" >> ~/.ssh/config
RUN echo "IdentityFile ~/.ssh/jprkey" >> ~/.ssh/config
RUN echo "ProxyJump server-test" >> ~/.ssh/config
RUN echo "Host server-prod" >> ~/.ssh/config
RUN echo "Hostname jpr-devops-prod.francecentral.cloudapp.azure.com" >> ~/.ssh/config
RUN echo "User jpr" >> ~/.ssh/config
RUN echo "IdentityFile ~/.ssh/jprkey" >> ~/.ssh/config
RUN echo "Host mongo-prod" >> ~/.ssh/config
RUN echo "Hostname 10.0.1.5" >> ~/.ssh/config
RUN echo "User jpr" >> ~/.ssh/config
RUN echo "ProxyJump server-prod" >> ~/.ssh/config
RUN echo "IdentityFile ~/.ssh/jprkey" >> ~/.ssh/config
  